import configparser
import pathlib
from typing import Any

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    app_name: str = "Awesome API"
    env: str = "dev-incode"
    mongo_url: str = "undefined"
    db_name: str = "undefined"

    def __init__(self, **values: Any):
        super().__init__(**values)
        current_dir = pathlib.Path(__file__).parent
        config = configparser.ConfigParser()
        config.read(str(current_dir) + '/application.ini')
        self.env = config.get('common','env')
        self.mongo_url = config.get('common', 'mongo_url')
        self.db_name = config.get('common', 'db_name')

    @staticmethod
    def get():
        return Settings()
