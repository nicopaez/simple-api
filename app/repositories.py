from typing import Annotated

import pymongo
from fastapi import Depends

from app.settings import Settings


class RepoFactory:
    @staticmethod
    def get_user_repo(settings: Annotated[Settings, Depends(Settings.get)]):
        return UserRepository(settings.mongo_url, settings.db_name)


class UserRepository:

    def __init__(self, mongo_url, db_name):
        self.db_url = mongo_url
        self.mongo_client = pymongo.MongoClient(mongo_url)
        self.db = self.mongo_client[db_name]

    def save(self, user):
        user.id = self.db.users.insert_one({"name": user.name}).inserted_id.__str__()
        return user

    def count(self, filter):
        return self.db.users.count_documents(filter)

