from fastapi import Depends, FastAPI
from typing import Annotated

from app.models import User
from app.repositories import UserRepository, RepoFactory
from app.settings import Settings

app = FastAPI()


@app.get("/")
async def root(settings: Annotated[Settings, Depends(Settings.get)]):
    env = settings.env
    return {"message": "Hello World", "env": env}


@app.post("/users")
async def create_person(user: User,
                        user_repository: Annotated[UserRepository, Depends(RepoFactory.get_user_repo)]):
    user_repository.save(user)
    return user
