from app.repositories import UserRepository
from testcontainers.mongodb import MongoDbContainer


def test_user_repository_count():
    with MongoDbContainer("mongo:latest") as mongo:
        # arrange: inserto un documento en mongo que esta dentro del testContainer
        conn = mongo.get_connection_client()
        mydb = conn.mydb
        result = mydb.users.insert_one({"name": "john"})

        # act: me conecto al mongo del testContainer y cuento los documentos
        mongo_url = "mongodb://test:test@" + conn.address[0] + ":" + str(conn.address[1])
        repo = UserRepository(mongo_url, mydb.name)

        # assert verifico que solo hay un documento
        assert repo.count({}) == 1