import os

from app.main import app
from app.settings import Settings
from fastapi.testclient import TestClient


class SettingsForTest(Settings):

    @staticmethod
    def get():
        settings = Settings()
        settings.env = "test"
        port = os.getenv("MONGO_TEST_PORT", "27017")
        settings.mongo_url = "mongodb://root:example@mongotest:" + port
        settings.db_name = "simpleapi"
        return settings


app.dependency_overrides[Settings.get] = SettingsForTest.get

client = TestClient(app)


def test_get_index():
    response = client.get("/")
    assert response.status_code == 200
    assert response.json() == {"message": "Hello World", "env": "test"}


def test_create_user():
    response = client.post(
        "/users/",
        json={"name": "some_name"},
    )
    assert response.status_code == 200
    assert response.json()["id"] is not None
