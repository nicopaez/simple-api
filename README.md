Simple API
==========

This is a sample project I created to show a way of implementing some ideas like Hexagonal Architecture, TDD, DDD among others with Python, FastAPI and other tools.

The project is structure with poetry so to start you must install poetry.

Here are some useful commands:

    # install dependencies
    poetry install

    # run test
    poetry run pytest

    # run the application
    uvicorn app.main:app --reload --host 0.0.0.0 

## Docker configuration

In order to simplify development a docker compose configuration is provided. In fact there are 2 compose configurations:

* docker-compose.yaml: is a completely standalone configuration, it includes mongo and python container, just start it and connect to the api container to start working.
* docker-compose.services.yaml: it contains just mongo, you must run python on your host.